## Progetto GoodCode

Questo progetto serve ad imparare le convenzioni di buona scrittura di codice.

Dopo la codeReview:

- nel file checkCode.md inserire il titolo della convenzione da impare;

- nel file approfondimentoCheckCode.md inserire nel dettaglio la convenzione, il bad code e, dopo averlo corretto, il good code.
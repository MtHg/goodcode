<style type="text/css">

@import url('https://fonts.googleapis.com/css?family=Righteous');

body{ 
    font-size: 20px;
    font-family: 'Righteous', cursive;
  }
td { 
    font-size: 8px;
}
h1.title {
    font-size: 38px;
    color: #FFF;
}
h1 { 
    font-size: 30px;
    color: #FFF;
}
h2 { 
    font-size: 25px;
    color: #FFF;
}
h3 { 
    font-size: 15px;
    color: #FFF;
}
code.r{ 
    font-size: 15px;
}
pre { 
    font-size: 14px;
}

p {
    text-align: justify;
}
</style>


# Approfondimento CheckCode - Loris -

> ## Mettere la Tendina

The ```html <select> ``` element, used along with one or more ```html <option>``` elements, creates a drop-down list of options for a web form. The ```html <select>``` element creates the list and each ```html <option>``` element is displayed as an available option in the list.
___
Es:

> ### Bad code:
```html

<table>
    <tr>
        <td>
            <input type="button" id="segnoPiu" name="segnoPiu" value=" &plus; " onClick="funzioneSomma()" />
        </td>
    </tr>

    <tr>
        <td>
            <input type="button" id="segnoMeno" name="segnoMeno" value=" &minus; " onClick="funzioneSottrazione()" />
        </td>
    </tr>

    <tr>
        <td>
            <input type="button" id="segnoPer" name="segnoPer" value=" &times; " onClick="funzioneMoltiplicatore()" />
        </td>
    </tr>

    <tr>
        <td>
            <input type="button" id="segnoDiviso" name="segnoDiviso" value=" &divide; " onClick="funzioneDivisione()" />
        </td>
    </tr>
</table>

```
> ### Good code:
```html

<td>
    <select name="operatore" class="operatore" id="operatore">
        <option value="addizione">&plus;</option>
        <option value="sottrazione">&minus;</option>
        <option value="moltiplicazione">&times;</option>
        <option value="divisione">&divide;</option>
    </select>
</td>

```
___

> ## Il risultato compare click su = 

The ```html <button>``` tag defines a clickable button.

Inside a ```html <button>``` element you can put content, like text or images. This is the difference between this element and buttons created with the ```html <input>``` element.
___
Es:

> ### Bad code:
```html

<td>
    <input type="button" id="uguale" name="uguale" value=" &equals; " disabled />
</td>

```
> ### Good code:
```html

<td>
    <button class="uguale" name="uguale" id="uguale">&equals;</button>
</td>
<td>
    <div id="risultato" class="risultato"></div>
</td>

```
___

> ## Layout

The Flexible Box Layout Module, makes it easier to design flexible responsive layout structure without using float or positioning.
___
Es:

> ### Bad code:
```css

body {
    height: 100%;
    padding: 0px;
}


```
> ### Good code:
```css

body {
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: space-between; 
}

```
___

> ## Non si utilizza onClick

The onclick event occurs when the user clicks on an element.
___
Es:

> ### Bad code:
```html

<tr>
    <td>
        <input type="button" id="segnoPiu" name="segnoPiu" value=" + " onClick="funzioneSomma()" />
    </td>
    <td>
        <input type="button" id="segnoMeno" name="segnoMeno" value=" - " onClick="funzioneSottrazione()" />
    </td>
    <td>
        <input type="button" id="segnoPer" name="segnoPer" value=" * " onClick="funzioneMoltiplicatore()" />
    </td>
    <td>
        <input type="button" id="segnoDiviso" name="segnoDiviso" value=" / " onClick="funzioneDivisione()" />
    </td>
    <td>
        <input type="button" id="uguale" name="uguale" value=" = " onClick=""/>
    </td>
</tr>


```
> ### Good code:
```html

<tr>
    <td>
        <select name="operatore" class="operatore" id="operatore">
            <option value="addizione">&plus;</option>
            <option value="sottrazione">&minus;</option>
            <option value="moltiplicazione">&times;</option>
            <option value="divisione">&divide;</option>
        </select>
    </td>
    <td>
        <input type="number" id="numero2" class="numero2" value="" placeholder="inserisci un numero" />
    </td>
    <td>
        <div id="risultato" class="risultato"></div>
    </td>
</tr>

```
___

> ## Si usa addEventListener

The addEventListener() method attaches an event handler to the specified element.

The addEventListener() method attaches an event handler to an element without overwriting existing event handlers.

___
Es:

> ### Bad code:
```javascript

var numero1, numero2, testo_risultato;

function funzioneSomma() {

    numero1 = Number(document.formCalc.num1.value); 
    numero2 = Number(document.formCalc.num2.value);
   
     testo_risultato = numero1 + numero2;   
     document.formCalc.risultato.value = testo_risultato;

}

function funzioneSottrazione() {

    numero1 = Number(document.formCalc.num1.value); 
    numero2 = Number(document.formCalc.num2.value);

    testo_risultato = numero1 - numero2;
    document.formCalc.risultato.value = testo_risultato;

}

function funzioneMoltiplicatore() {

    numero1 = Number(document.formCalc.num1.value); 
    numero2 = Number(document.formCalc.num2.value);

    testo_risultato = numero1 * numero2;
    document.formCalc.risultato.value = testo_risultato;

}

function funzioneDivisione() {

    numero1 = Number(document.formCalc.num1.value); 
    numero2 = Number(document.formCalc.num2.value);

    if(numero2 === 0)
        console.log(infinity)
    else {

        testo_risultato = numero1 / numero2;
        document.formCalc.risultato.value = testo_risultato;

    }
        
}


```
> ### Good code:
```javascript

document.addEventListener("DOMContentLoaded", () => {
    document.getElementById("uguale").addEventListener('click', calcolaOperatore );
})

function calcolaOperatore() {

    let testo1 = document.querySelector('#numero1').value;
    let testo2 = document.querySelector('#numero2').value;
    let operatore = document.querySelector('#operatore').value;
    let calcola;

    if(testo1 === '' ) {
        alert('Inserimento operatore 1 non è valido!')
        return;
    }

    if(testo2 === '') {
        alert('Inserimento operatore 2 non è valido!')
        return;
    }


    if(isNaN(testo1)) {
        alert('Inserimento operatore 1 non è corretto!')
        return;
    }

    if(isNaN(testo2)) {
        alert('Inserimento operatore 2 non è corretto!')
        return;
    }

    let numero1 = parseFloat(testo1);
    let numero2 = parseFloat(testo2);

    if(operatore === "addizione") 
        calcola = numero1 + numero2;
    else if(operatore === "sottrazione")
        calcola = numero1 - numero2;
    else if(operatore === "divisione") {
        if(numero1 != '0' && numero2 == '0') {
            alert('Impossibile dividere per 0!');
        } else 
            calcola = numero1 / numero2;
    } 
    
    else if (operatore === "moltiplicazione") {
            calcola = numero1 * numero2;
    } 
    
    else {
    }

    document.querySelector('#risultato').innerHTML = calcola;
}

```
___

> ## Non si usa var 

L'ambiente di una variabile dichiarata con var è il suo attuale contesto di esecuzione, che è la funzione di chiusura o, per le variabili dichiarate al di fuori di qualsiasi funzione, globale. Se si dichiara nuovamente una variabile JavaScript, il suo valore non sarà perso.

Assegnando un valore a una variabile non dichirata la rende implicitamente globale (diventa una proprietà dell'oggetto) quando viene eseguita.
___
Es:

> ### Bad code:
```javascript

var a = 6;
console.log(a);
  // possibile output: 6

var b = 5;
console.log(b);
  // possibile output: 5

var c = 4;
console.log(c);
  // possibile output: 4

```
> ### Good code:
```javascript

let a = 6;
console.log(a);
  // possibile output: 6

const b = 5;
console.log(b);
  // possibile output: 5

typeof c = 4;
console.log(c);
  // possibile output: 4

```
___

> ## Non si usa innerHTML

The innerHTML property sets or returns the HTML content (inner HTML) of an element. Every time innerHTML is set, the HTML has to be parsed, a DOM constructed, and inserted into the document.
___
Es:

> ### Bad code:
```javascript

function calcolaOperatore() {
    var a = parseFloat(document.calcolatrice.querySelector('#numero1').value);
    var b = parseFloat(document.calcolatrice.querySelector('#numero2').value);
    var operatore = document.calcolatrice.querySelector('#operatore').value;
    var calcola;

    if(operatore === "addizione") {
        calcola = a + b;
    } else if(operatore === "sottrazione") {
        calcola = a - b;
    } else if(operatore === "divisione") {
        if(b == '0') {
            console.log('Infinity');
        } else {
            calcola = a / b;
        }
    } else if (operatore === "moltiplicazione") {
        calcola = a * b;
    } 

    console.log(calcola);

    document.calcolatrice.querySelector('#risultato').innerHTML = calcola;
}


```
> ### Good code:
```javascript

document.addEventListener("DOMContentLoaded", () => {
    document.getElementById("operatore").addEventListener('change', calcolaRisultato );
    document.getElementById("numero1").addEventListener('keyup', calcolaRisultato );
    document.getElementById("numero2").addEventListener('keyup', calcolaRisultato );
})

function calcolaRisultato()
{
    let errore = document.getElementById('error');
    let testo1 = document.getElementById("numero1").value;
    let testo2 = document.getElementById("numero2").value;

    errore.innerText = '';

    if(testo1 === '')
    {
        errore.innerText = 'Il primo operando non è stato inserito';
        return;
    }

    if(testo2 === '')
    {
       errore.innerText = 'Il secondo operando non è stato inserito';
        return;
    }

    if(isNaN(testo1))
    {
        errore.innerText = 'Il valore dell\' operando 1 non è valido';
        return;
    }
        

    else if(isNaN(testo2))
    {
        errore.innerText = 'Il valore dell\' operando 2 non è valido';
        return;
    }
    

    let operatore = document.getElementById("operatore").value;

    let risultato = 0;
    let num1 = parseFloat(testo1);
    let num2 = parseFloat(testo2);

    if(operatore === 'addizione')
        risultato = num1 + num2;

    else if(operatore === 'sottrazione')
        risultato = num1 - num2;

    else if(operatore === 'moltiplicazione')
         risultato = num1 * num2;

    else
    {
        if(num2 === 0)
        {
            testo2 = '';
            alert("Impossibile dividere per 0");
            return;
        }

        else
            risultato = num1/num2;
    }

    document.getElementById('risultato').value = risultato;


}

```
___

> ## Non si usa la nomenclatura dei form 

This is a set of rules I set for our internal JavaScript style guide after finding nothing similar online. Having consulted a few of my colleagues and used these extensively myself, I stand by these guidelines and use them every day. I hope you find these either helpful or at least controversial. These rules are optimized for clarity and readability above all. They favour the code reviewer or code maintainer over the code author. Eventually, we are all the maintainer
___
Es:

> ### Bad code:
```html

<tr>
    <td>
        <input type="button" name="uno" id="uno" value="1" onclick="calcolatrice.display.value += '1'">
    </td>

    <td>
        <input type="button" name="due" id="due" value="2" onclick="calcolatrice.display.value += '2'">
    </td>

    <td>
        <input type="button" name="tre" id="tre" value="3" onclick="calcolatrice.display.value += '3'">
    </td>

    <td>
        <input type="button" class="operatore" name="piu" id="operatore" value="+" onclick="calcolatrice.display.value += '+'">
    </td>

    <td>
        <input type="button" class="operatore" name="meno" id="operatore" value="-" onclick="calcolatrice.display.value += '-'">
    </td>
</tr>

<!-- riga4 -->

<tr>

    <td>
        <input type="button" name="quattro" id="quattro" value="4" onclick="calcolatrice.display.value += '4'">
    </td>

    <td>
        <input type="button" name="cinque" value="5" id="cinque" onclick="calcolatrice.display.value += '5'">
    </td>

    <td>
        <input type="button" name="sei" value="6" id="sei" onclick="calcolatrice.display.value += '6'">
    </td>

    <td>
        <input type="button" class="operatore" name="per" value="*" id="operatore" onclick="calcolatrice.display.value += '*'">
    </td>

    <td>
        <input type="button" class="operatore" name="diviso" value="/" id="operatore" onclick="calcolatrice.display.value += '/'">
    </td>
</tr>

<!-- riga5 -->

<tr>
    <td>
        <input type="button" name="sette" value="7" id="sette" onclick="calcolatrice.display.value += '7'">
    </td>

    <td>
        <input type="button" name="otto" value="8" id="otto" onclick="calcolatrice.display.value += '8'">
    </td>

    <td>
        <input type="button" name="nove" value="9" id="nove" onclick="calcolatrice.display.value += '9'">
    </td>

    <td>
        <input type="button" name="zero" value="0" id="zero" onclick="calcolatrice.display.value += '0'">
    </td>

    <td>
        <input type="button" name="uguale" value="=" id="uguale" onclick="calcolatrice.display.value = eval(calcolatrice.display.value)">
    </td>
</tr>


```
> ### Good code:
```html

<tr>
    <td colspan="3">
        <input type="text" name="testo" id="testo" class="testo" disabled />
    </td>
    <td colspan="2">
        <input type="text" name="testo_operatore" id="testo_operatore" class="testo_operatore" disabled />
    </td>
</tr>

<!-- riga2 -->

<tr>

    <td colspan="3">
        <input type="text" name="testo2" id="testo2" class="testo2" disabled />
    </td>

    <td colspan="2">
        <input type="text" class="testo_uguale" value="" disabled />
    </td>

</tr>

<!-- riga3 -->

<tr>

    <td colspan="5">
        <input type="text" name="risultato" id="risultato" class="risultato" disabled />
    </td>

</tr>

<!-- riga4 -->

<tr>
    <td>
        <input type="button" name="uno" id="uno" class="uno" value="1" > 
    </td>

    <td>
        <input type="button" name="due" id="due" class="due" value="2" >
    </td>

    <td>
        <input type="button" name="tre" id="tre" class="tre" value="3" >
    </td>

    <td>
        <input type="button" class="operatore1" name="piu" id="operatore1" value="&plus;" >
    </td>

    <td>
        <input type="button" class="operatore2" name="meno" id="operatore2" value="&minus;" >
    </td>
</tr>

<!-- riga5 -->

<tr>

    <td>
        <input type="button" name="quattro" id="quattro" class="quattro" value="4" >
    </td>

    <td>
        <input type="button" name="cinque" value="5" id="cinque" class="cinque" >
    </td>

    <td>
        <input type="button" name="sei" value="6" id="sei" class="sei" >
    </td>

    <td>
        <input type="button" class="operatore3" name="per" value="&times;" id="operatore3" >
    </td>

    <td>
        <input type="button" class="operatore4" name="diviso" value="&divide;" id="operatore4" >
    </td>
</tr>

<!-- riga6 -->

<tr>
    <td>
        <input type="button" name="sette" value="7" id="sette" class="sette" > 
    </td>

    <td>
        <input type="button" name="otto" value="8" id="otto" class="otto" >
    </td>

    <td>
        <input type="button" name="nove" value="9" id="nove" class="nove" >
    </td>

    <td>
        <input type="button" name="zero" value="0" id="zero" class="zero" > 
    </td>

    <td>
        <input type="button" name="uguale" value="&equals;" id="uguale" class="uguale" >
    </td>
</tr>

```
___

> ## Si usa sempre === 

That's right, it's not a typo, but 3 equal signs, and it means equality without type coersion. In other words, if using the triple equals, the values must be equal in type as well.
___
Es:

> ### Bad code:
```javascript

if(operatore == "addizione") {
    calcola = a + b;
} else if(operatore == "sottrazione") {
    calcola = a - b;
} else if(operatore == "divisione") {
    if(b == '0') {
        console.log('Infinity');
    } else {
        calcola = a / b;
    }
} else if (operatore == "moltiplicazione") {
    calcola = a * b;
}

document.calcolatrice.querySelector('#risultato').innerHTML = calcola;


```
> ### Good code:
```javascript

if(operatore === "addizione") 
        calcola = numero1 + numero2;
else if(operatore === "sottrazione")
    calcola = numero1 - numero2;
else if(operatore === "divisione") {
    if(numero1 !== '0' && numero2 === '0') {
        alert('Impossibile dividere per 0!');
    } else 
        calcola = numero1 / numero2;
} 

else if (operatore === "moltiplicazione") {
        calcola = numero1 * numero2;
}

document.querySelector('#risultato').innerHTML = calcola;

```
___

> ## Non si manipola il css

___
Es:

> ### Bad code:
```css

#numero1, #numero2, #uguale {

    font-size: 20px;
    text-align: center;
    border: 2px solid #FEE;
    background: transparent;
    border-radius: 20px;

}
```
```javascript
var a = parseFloat(document.calcolatrice.querySelector('#numero1').value);
var b = parseFloat(document.calcolatrice.querySelector('#numero2').value);
var operatore = document.calcolatrice.querySelector('#uguale').value;

```
> ### Good code:
```css

.numero1, .numero2, .operatore, .uguale, .risultato, .error {

    
    border: 2px solid #FFF;
    border-radius: 20px;
    background: #FFF;

    font-family: 'Shadows Into Light', cursive;
    font-size: 30px;
    text-align: center;

}
```
```javascript

document.getElementById("operatore").addEventListener('change', calcolaRisultato );
document.getElementById("numero1").addEventListener('keyup', calcolaRisultato );
document.getElementById("numero2").addEventListener('keyup', calcolaRisultato );

```
___

> ## Si aggiungono tolgono classi

___
Es:

> ### Bad code:
```javascript

var testo1 = parseFloat(document.calcolatrice.querySelector('#numero1').value);
var testo2 = parseFloat(document.calcolatrice.querySelector('#numero2').value);
var errore = document.calcolatrice.querySelector('#errore').value;


```
> ### Good code:
```javascript

let errore = document.getElementById('error');
let testo1 = document.getElementById("numero1").value;
let testo2 = document.getElementById("numero2").value;

```
___

> ## Per i stili si usano solo classi 

Changing a class is one of the most often used actions in scripts.

In the ancient time, there was a limitation in JavaScript: a reserved word like "class" could not be an object property. That limitation does not exist now, but at that time it was impossible to have a "class" property, like elem.class.

___
Es:

> ### Bad code:
```css

#risultato {

    font-size: 20px; 
    text-align: center;

}

#num1, #num2, #segnoPiu, #segnoMeno, #segnoPer, #segnoDiviso, #uguale {

    font-size: 20px;
    text-align: center;
    border: 2px solid #FEE;
    background: transparent;
    border-radius: 20px;

}

#myForm {

    margin: 0px;
    
}

#myCalc {

    text-align: center;
    font-size: 30px;

}


```
> ### Good code:
```javascript

.risultato {

    font-size: 25px;
    border: 2px solid #fff;
    margin-left: 25px;
    font-family: 'Warnes', cursive;
    
}

.operatore, .uguale {

    font-size: 35px;

    border: 2px solid transparent;
    border-radius: 20px;
    background: #FFF;
    text-align: center;
    font-family: 'Warnes', cursive;

}


.numero1, .numero2 {

    font-size: 25px;

    border: 2px solid transparent;
    border-radius: 20px;
    background: #FFF;
    text-align: center;
    font-family: 'Warnes', cursive;

}

input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button {

    -webkit-appearance: none; 
    margin: 0; 

}

.myTable {

    margin: 30px;
    margin-left: 10%;
    margin-right: 30%;

}


.titolo_versione1 {

    font-family: 'Warnes', cursive;
    text-align: center;

}

.visibile { display: block;}

.errore { display: none; color: red}

```
___

> ## Non si usano i character

So the first problem is that HTML can not wrap your text where that space is. This is sort of ok if you use one regular space and one ```html &nbsp; ``` because it can still wrap text on the regular space. However if you put the regular space first and then the ```html &nbsp; ``` you can end up with a space at the beginning of the line after it wraps. If you do use this method, you have to make sure the regular space comes second (although even this is a problem if you are using justified text).
___
Es:

> ### Bad code:
```javascript

if(testo1 === '')
    {
        errore.innerText = 'Il primo operando non &egrave; stato inserito';
        return;
    }

    if(testo2 === '')
    {
       errore.innerText = 'Il secondo operando non &egrave; stato inserito';
        return;
    }

    if(isNaN(testo1))
    {
        errore.innerText = 'Il valore dell\' operando 1 non &egrave; valido';
        return;
    }
        

    else if(isNaN(testo2))
    {
        errore.innerText = 'Il valore dell\' operando 2 non &egrave; valido';
        return;
    }

```
> ### Good code:
```javascript

if(testo1 === '')
    {
        errore.innerText = 'Il primo operando non è stato inserito';
        return;
    }

    if(testo2 === '')
    {
       errore.innerText = 'Il secondo operando non è stato inserito';
        return;
    }

    if(isNaN(testo1))
    {
        errore.innerText = 'Il valore dell\' operando 1 non è valido';
        return;
    }
        

    else if(isNaN(testo2))
    {
        errore.innerText = 'Il valore dell\' operando 2 non è valido';
        return;
    }

```
___

> ## Settare meta

The charset attribute specifies the character encoding for the HTML document.

The charset attribute can be locally overridden using the lang attribute on any element.
___
Es:

> ### Bad code:
```html

<head>
    <title>Calcolatrice Versione2</title>
    <link rel="stylesheet" type="text/css" href="index.css" />
    <script src="index.js"></script>
</head>


```
> ### Good code:
```html

<head>
    <title>Calcolatrice Versione2</title>
    <link rel="stylesheet" type="text/css" href="index.css" />
    <script src="index.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Shadows+Into+Light" rel="stylesheet">
    <meta charset="UTF-8" />
</head>

```
___

> ## suddividere sempre per metodi ogni metodo deve fare una cosa sola

___
Es:

> ### Bad code:
```javascript

var numero1, numero2, testo_risultato;

function funzioneSomma() {

    numero1 = Number(document.formCalc.num1.value); 
    numero2 = Number(document.formCalc.num2.value);
   
     testo_risultato = numero1 + numero2;   
     document.formCalc.risultato.value = testo_risultato;

}

function funzioneSottrazione() {

    numero1 = Number(document.formCalc.num1.value); 
    numero2 = Number(document.formCalc.num2.value);

    testo_risultato = numero1 - numero2;
    document.formCalc.risultato.value = testo_risultato;

}

function funzioneMoltiplicatore() {

    numero1 = Number(document.formCalc.num1.value); 
    numero2 = Number(document.formCalc.num2.value);

    testo_risultato = numero1 * numero2;
    document.formCalc.risultato.value = testo_risultato;

}

function funzioneDivisione() {

    numero1 = Number(document.formCalc.num1.value); 
    numero2 = Number(document.formCalc.num2.value);

    if(numero2 === 0)
        console.log(infinity)
    else {

        testo_risultato = numero1 / numero2;
        document.formCalc.risultato.value = testo_risultato;

    }
        
}

```
> ### Good code:
```javascript

function calcolaRisultato()
{
    let errore = document.getElementById('error');
    let testo1 = document.getElementById("numero1").value;
    let testo2 = document.getElementById("numero2").value;

    errore.innerText = '';

    if(testo1 === '')
    {
        errore.innerText = 'Il primo operando non è stato inserito';
        return;
    }

    if(testo2 === '')
    {
       errore.innerText = 'Il secondo operando non è stato inserito';
        return;
    }

    if(isNaN(testo1))
    {
        errore.innerText = 'Il valore dell\' operando 1 non è valido';
        return;
    }
        

    else if(isNaN(testo2))
    {
        errore.innerText = 'Il valore dell\' operando 2 non è valido';
        return;
    }
    

    let operatore = document.getElementById("operatore").value;

    let risultato = 0;
    let num1 = parseFloat(testo1);
    let num2 = parseFloat(testo2);

    if(operatore === 'addizione')
        risultato = num1 + num2;

    else if(operatore === 'sottrazione')
        risultato = num1 - num2;

    else if(operatore === 'moltiplicazione')
         risultato = num1 * num2;

    else
    {
        if(num2 === 0)
        {
            testo2 = '';
            alert("Impossibile dividere per 0");
            return;
        }

        else
            risultato = num1/num2;
    }

    document.getElementById('risultato').value = risultato;


}

```
___

> ## Eval is Evil :smiling_imp:

When people say "eval is evil," they usually mean that "dynamic code execution is a very powerful thing, and also very easy to get wrong." It's specially problematic when the interpreter takes a plain string, or any other form of input where we can't guarantee the proper structure and behaviour of the resulting program in case it's created dynamically. Additionally, when the eval code has access to the current environment, or can't otherwise be sandboxed (specially in an impure language) — as is the case with most implementations —, an interpreted program might do things that you don't want it to do.

___
Es:

> ### Bad code:
```html
<tr>
    <td>
        <input type="button" name="uguale" value="=" id="uguale" onclick="calcolatrice.display.value = eval(calcolatrice.display.value)">
    </td>
</td>


```
> ### Good code:
```javascript

   document.addEventListener("DOMContentLoaded",  () => {

    let testo;
    let testo2;

    testo = document.getElementById("#testo");
    testo2 = document.getElementById("#testo2");

    let operazione;
    operazione = document.getElementById("#testo_operatore");
    
    let testo_risultato;
    testo_risultato = document.getElementById("#risultato");

    const impostaOperatore = (primoElemento, tipo_operatore) => 
    document.getElementById(primoElemento).addEventListener('click', function inserisciOperatore() { document.calcolatrice.value += tipo_operatore});

    impostaOperatore("operatore1", "+");
    impostaOperatore("operator2", "-");
    impostaOperatore("operator3", "*");
    impostaOperatore("operatore4", "/");
    impostaOperatore("uguale", "=");
    impostaOperatore("cancella", "");

    switch(tipo_operatore) {

        case "+":
            operazione.innerText = '+';
            risultato = testo + testo2;
            risultato.innerText = testo_risultato;
        break;
        case "-":
            operazione.innerText = '-';
            risultato = testo - testo2;
            risultato.innerText = testo_risultato;
        break;
        case "*":
            operazione.innerText = '*';
            risultato = testo * testo2;
            risultato.innerText = testo_risultato;
        break;
        case "/":
            operazione.innerText = '/';

                if(testo2 === '0')
                    alert('impossibile dividere per 0!');
                else
                    risultato = testo / testo2;

            risultato.innerText = testo_risultato;
        break;
        case "=":
            operazione.innerText = '=';
        break;
        default:
            operazione.innerText = '';
        break;
    }


    [["operatore1", "+" ], ["operatore2", "-" ], ["operatore3", "*" ], ["operatore4", "/" ], ['uguale', "="], ['cancella', "c"]].forEach((operatore_tipo) => impostaOperatore(operatore_tipo[0], operatore_tipo[1], operatore_tipo[2], operatore_tipo[3]));
   
})

```
___
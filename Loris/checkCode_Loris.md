<style type="text/css">

@import url('https://fonts.googleapis.com/css?family=Righteous');

body{ 
    font-size: 20px;
    font-family: 'Righteous', cursive;
  }
td { 
    font-size: 8px;
}
h1.title {
    font-size: 38px;
    color: #FFF;
}
h1 { 
    font-size: 30px;
    color: #FFF;
}
h2 { 
    font-size: 25px;
    color: #FFF;
}
h3 { 
    font-size: 15px;
    color: #FFF;
}
code.r{ 
    font-size: 15px;
}
pre { 
    font-size: 14px;
}

p {
    text-align: justify;
}
</style>

# Check Code - Loris -

> ### [Mettere la tendina;](approfondimentoCheckCode_Loris.md#mettere-la-tendina)

> ### [Il risultato compare click su = ;](approfondimentoCheckCode_Loris.md#il-risultato-compare-click-su-=-;)

> ### [Layout deve essere fatto con <div style "display:flex;"></div>;](approfondimentoCheckCode_Loris.md#layout)

> ### [Non si utilizza onClick (no JS in HTML);](approfondimentoCheckCode_Loris.md#non-si-utilizza-onClick)

> ### [Si usa AddEventListener();](approfondimentoCheckCode_Loris.md#si-usa-addEventListener)

> ### [Non si usa var, usare sempre const (se è proprio necessario usare let);](approfondimentoCheckCode_Loris.md#non-si-usa-var)

> ### [Non si usa assolutamente innerHTML (usare innerText/value);](approfondimentoCheckCode_Loris.md#non-si-usa-innerHTML)

> ### [Non si usa la nomenclatura dei form per ottenere valori;](approfondimentoCheckCode_Loris.md#non-si-usa-la-nomenclatura-dei-form)

> ### [Si usa sempre === (no ==);](approfondimentoCheckCode_Loris.md#si-usa-sempre-===)

> ### [Non si manipola il css (style);](approfondimentoCheckCode_Loris.md#non-si-manipola-il-css)

> ### [Si aggiungono/tolgono class CSS;](approfondimentoCheckCode_Loris.md#si-aggiungono-tolgono-classi)

> ### [Per stili si usano solo .classi (no #Id);](approfondimentoCheckCode_Loris.md#per-i-stili-si-usano-solo-classi)

> ### [Non si utilizzano character-entity (es. &egrave) nel javascript, nè html;](approfondimentoCheckCode_Loris.md#non-si-utilizzano-character)

> ### [Settare <meta charset "UTF-8/16" /> e salvare i js;](approfondimentoCheckCode_Loris.md#settare-meta)

> ### [Suddividere sempre per metodi ogni metodo deve fare una cosa sola;](approfondimentoCheckCode_Loris.md#suddividere-sempre-per-metodi-ogni-metodo-deve-fare-una-cosa-sola)

> ### [Eval is Evil (NO);](approfondimentoCheckCode_Loris.md#eval-is-evil)


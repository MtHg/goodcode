# Good code conventions: further informations

## Nomenclatura chiara e concisa:

Gli identificatori utilizzati per le variabili, per le funzioni, per gli oggetti, ecc., devono rappresentare al meglio il contenuto di ciò che li segue. Questo vuol dire che la nomenclatura deve essere chiara, concisa ma senza abbreviazioni, deve parlare per noi nella lettura del codice.
___
Es:

bad code:
```javascript
let C = document.getElementById("C");
```
good code:
```javascript
const deleteButton = document.getElementById("deleteButton");
```
___
Es:

bad code:
```javascript
numeri.forEach(function(elemento)){
    //some code
}
```
good code:
```javascript
const numeri = document.querySelectorAll(".numero");
numeri.forEach(function digitazioneNumeri(elemento)){
    //same code as above
}
```
___
___
## Ridurre lo scope delle variabili:

La dichiarazione delle variabili deve avvenire il più possibile vicino a dove le variabili stesse vengono utilizzate. Le funzioni sono associate ad un ambiente, per cui quando una funzione è applicata a degli argomenti(le nostre variabili ad esempio), il suo ambiente ha sempre una reference al "super-ambiente" e alle variabili mappate in esso.
___
Es:

bad code:
```javascript
//dichiaro la variabile simboloOperazione
let simboloOperazione =  document.getElementById("simboloOperazione");

//tanto codice nel mezzo

operazioni.forEach(function visualizzaSimboloOperazione(simbolo){
    simbolo.addEventListener("click",function(){

        //aggiornamento variabile
        simboloOperazione = document.getElementById("simboloOperazione");

        let primoNumero = document.getElementById("primoNumero");

        //uso la variabile simboloOperazione
        if(primoNumero.value !== "" && (simboloOperazione.value == undefined|| simboloOperazione.value == "")){
            simboloOperazione.value = simbolo.value;
        }
    })
});
```
good code:
```javascript
operazioni.forEach(function visualizzaSimboloOperazione(simbolo){
    simbolo.addEventListener("click",function visualizzaSimboloOperazione(){

        //dichiaro la variabile simboloOperazione
        const simboloOperazione = document.getElementById("simboloOperazione");

        const primoNumero = document.getElementById("primoNumero");

        //uso la variabile simboloOperazione
        if(primoNumero.value !== "" && (simboloOperazione.value == undefined|| simboloOperazione.value == "")){
            simboloOperazione.value = simbolo.value;
        }
    })
});
```
___
___
## Suddividere le responsabilità:

Una funzione deve svolgere possibilmente un solo compito, il che implica la suddivisione delle responsabilità nella preparazione dei dati, nella validazione e nel calcolo.
___
Es:

bad code:
```javascript
function calcolaRisultato(){
    primoNumero = Number(document.getElementById("primoNumero").value);
    secondoNumero = Number(document.getElementById("secondoNumero").value);
    selezioneOperazione = document.getElementById("operazione");
    operazione = selezioneOperazione.options[selezioneOperazione.selectedIndex].value;

    if(operazione == "+"){
        risultato = primoNumero + secondoNumero;
    }
    //..altro codice..
    visualizzazioneRisultato.value = risultato;
}
```
good code:
```javascript
//preparazione dati
function getNumbers(){
    const primoNumero = Number(document.getElementById("primoNumero").value);
    const secondoNumero = Number(document.getElementById("secondoNumero").value);

    return [primoNumero,secondoNumero];
}

//preparazione dati
function getOperation(){
    const selezioneOperazione = document.getElementById("operazione");
    const operazione = selezioneOperazione.options[selezioneOperazione.selectedIndex].value;

    return operazione;
}

//validazione
function calcolaRisultato (){
    const arrayValues = getNumbers();
    const operazione = getOperation();
    let risultato;

    switch(operazione){
        case "+":
            risultato = sum(arrayValues);
            break;
        //..altro codice..
	}
	return risultato;
}

//calcolo
function sum (arrayValues){
    let result = 0;
    for (value of arrayValues){
        result = result + Number(value);
    }
    return result;
}
```
___
___
## Evitare lo stato mutabile di variabili:
Nella dichiarazione delle variabili è bene utilizzare la keyword **_const_**, quando possibile, per evitare lo stato mutabile delle variabili stesse. In pratica, è da evitare la riassegnazione delle stesse variabili a valori diversi.
___
Es:

bad code:
```javascript
let primoNumero;

//..codice..

function calcolaRisultato(){
    primoNumero = Number(document.getElementById("primoNumero").value);
    //..altro codice..
}
```
good code:
```javascript
function getNumbers(){
    const primoNumero = Number(document.getElementById("primoNumero").value);
    //..codice..
}
```
___
___

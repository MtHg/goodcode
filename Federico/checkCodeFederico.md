## Good code conventions:

- ### [Nomenclatura chiara e concisa](approfondimentoCheckCodeMT.md#nomenclatura-chiara-e-concisa)

- ### [Ridurre lo scope delle variabili](approfondimentoCheckCodeMT.md#ridurre-lo-scope-delle-variabili)

- ### [Suddividere le responsabilità](approfondimentoCheckCodeMT.md#suddividere-le-responsabilità)

- ### [Evitare lo stato mutabile di variabili](approfondimentoCheckCodeMT.md#evitare-lo-stato-mutabile-di-variabili)
